package br.ucsal.bes.poo20191.domain;

public class Enfermeiros extends Funcionarios {

	private Integer coren;
	private Boolean pos;

	@Override
	public String toString() {
		return "Enfermeiros [coren=" + coren + ", pos=" + pos + "]";
	}

	public Enfermeiros(Integer matricula, String nome, Double salBase, Integer coren, Boolean pos)
			throws SalarioNaoValidoException {
		super(matricula, nome, salBase);
		this.coren = coren;
		this.pos = pos;
	}

	public Integer getCoren() {
		return coren;
	}

	public void setCoren(Integer coren) {
		this.coren = coren;
	}

	public Boolean getPos() {
		return pos;
	}

	public void setPos(Boolean pos) {
		this.pos = pos;
	}

	@Override
	public Double gratificacao(){
		Double gratificacao;
			
		if (pos = false) {
			gratificacao = salBase + (salBase * 0.03);

		} else {
			gratificacao = salBase + (salBase * 0.08);
		}
		return gratificacao;

	}

}
