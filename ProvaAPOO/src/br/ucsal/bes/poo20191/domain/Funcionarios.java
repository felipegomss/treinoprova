package br.ucsal.bes.poo20191.domain;

public abstract class Funcionarios {

	private Integer matricula;
	private String nome;
	protected Double salBase;

	@Override
	public String toString() {
		return "Funcionarios [matricula=" + matricula + ", nome=" + nome + ", salBase=" + salBase + "]";
	}

	public Funcionarios(Integer matricula, String nome, Double salBase) throws SalarioNaoValidoException {
		super();
		this.matricula = matricula;
		this.nome = nome;
		if (salBase <= 0 || salBase > 30000) {
			throw new SalarioNaoValidoException ("O Sal�rio n�o pode ser MENOR ou IGUAL a 0, nem MAIOR que 30000");
		}	
		this.salBase = salBase;
	}
	
	public abstract Double gratificacao(); 

	public Integer getMatricula() {
		return matricula;
	}

	public void setMatricula(Integer matricula) {
		this.matricula = matricula;
	}

	public String getNome() {
		return nome;
	}

	public void setNome(String nome) {
		this.nome = nome;
	}

	public Double getSalBase(){
		return salBase;
	}

	public void setSalBase(Double salBase) throws SalarioNaoValidoException {
		if (salBase <= 0 || salBase > 30000) {
			throw new SalarioNaoValidoException ("O Sal�rio n�o pode ser MENOR ou IGUAL a 0, nem MAIOR que 30000");
		}	
		this.salBase = salBase;
	}

}
