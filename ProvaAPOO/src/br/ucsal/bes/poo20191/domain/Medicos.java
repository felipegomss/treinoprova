package br.ucsal.bes.poo20191.domain;

import java.util.ArrayList;
import java.util.List;

public class Medicos extends Funcionarios {

	private static List<String> especialidade = new ArrayList<>();
	private Integer crm;

	@Override
	public String toString() {
		return "Medicos [especialidade=" + especialidade + ", CRM=" + crm + "]";
	}

	public Medicos(Integer matricula, String nome, Double salBase, List<String> especialidade, Integer crm)
			throws SalarioNaoValidoException {
		super(matricula, nome, salBase);
		this.especialidade = especialidade;
		this.crm = crm;
	}
	public static List<String> addEspecialidade(String string) {
		String especialidades = new String();
		especialidade.add(especialidades);
		return especialidade;
	}

	public List<String> getEspecialidade() {
		return especialidade;
	}

	public void setEspecialidade(List<String> especialidade) {
		this.especialidade = especialidade;
	}

	public Integer getCRM() {
		return crm;
	}

	public void setCRM(Integer cRM) {
		crm = cRM;
	}

	@Override
	public Double gratificacao(){		
		Double gratificacao = salBase * (especialidade.size() * 0.10);
		return gratificacao;
	}

}
