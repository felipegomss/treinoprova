package br.ucsal.bes.poo20191.domain;

public class SalarioNaoValidoException extends Exception{

	private static final long serialVersionUID = 1L;
	
	public SalarioNaoValidoException(String message) {
		super(message);
	}

}
