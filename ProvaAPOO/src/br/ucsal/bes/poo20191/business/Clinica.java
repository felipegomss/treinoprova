package br.ucsal.bes.poo20191.business;

import java.util.ArrayList;
import java.util.Comparator;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

import br.ucsal.bes.poo20191.domain.Funcionarios;
import br.ucsal.bes.poo20191.domain.Medicos;
import br.ucsal.bes.poo20191.domain.SalarioNaoValidoException;

public class Clinica {

	private static List<Funcionarios> funcionarios = new ArrayList<>();

	public static void cadastrarMedico(Integer matricula, String nome, Double salBase, List<String> especialidade,
			Integer crm) throws SalarioNaoValidoException {

		Medicos medico = new Medicos(crm, nome, salBase, especialidade, crm);

		if (salBase <= 0 || salBase > 30000) {
			throw new SalarioNaoValidoException("O Sal�rio n�o pode ser MENOR ou IGUAL a 0, nem MAIOR que 30000");
		}

		funcionarios.add(medico);
	}
	
	public static void addEspecialidade(List<String> especialidade) {
		String especialidades = new String();
		especialidade.add(especialidades);
	}

	public static void listarFuncionariosOrdenadoSalarioBase(List<Funcionarios> funcionarios) {
		funcionarios.sort(new Comparator<Funcionarios>() {
			@Override
			public int compare(Funcionarios arg0, Funcionarios arg1) {
				int comparacao = arg0.getSalBase().compareTo(arg1.getSalBase());
				return comparacao;
			}
		});

		System.out.println("Funcion�rios por ordem crescente de sal�rio base:");
		for (Funcionarios funcionarios2 : funcionarios) {
			System.out.println(funcionarios + "\n");
		}

	}

	public static void listarEspecialidades(List<String> especialidade) {
		Set<String> especialidades = new HashSet<>(especialidade);

		System.out.println("Especialidades dos m�dicos da cl�nica:");
		for (String string : especialidades) {
			System.out.println(especialidades + "\n");
		}

	}

}
